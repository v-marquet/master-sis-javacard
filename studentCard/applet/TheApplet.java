package applet;


import javacard.framework.*;




public class TheApplet extends Applet {


	static final byte UPDATECARDKEY                  = (byte)0x14;
	static final byte UNCIPHERFILEBYCARD             = (byte)0x13;
	static final byte CIPHERFILEBYCARD               = (byte)0x12;
	static final byte CIPHERANDUNCIPHERNAMEBYCARD    = (byte)0x11;
	static final byte READFILEFROMCARD               = (byte)0x10;
	static final byte WRITEFILETOCARD                = (byte)0x09;
	static final byte UPDATEWRITEPIN                 = (byte)0x08;
	static final byte UPDATEREADPIN                  = (byte)0x07;
	static final byte DISPLAYPINSECURITY             = (byte)0x06;
	static final byte DESACTIVATEACTIVATEPINSECURITY = (byte)0x05;
	static final byte ENTERREADPIN                   = (byte)0x04;
	static final byte ENTERWRITEPIN                  = (byte)0x03;
	static final byte READNAMEFROMCARD               = (byte)0x02;
	static final byte WRITENAMETOCARD                = (byte)0x01;

	final static short  SW_VERIFICATION_FAILED       = (short) 0x6320;
	final static short  SW_PIN_VERIFICATION_REQUIRED = (short) 0x6301;

	static final short FILE_TRANSFERT_PAYLOAD_SIZE = 127;

	final static short NVRSIZE      = (short)1024;
	static byte[] NVR               = new byte[NVRSIZE];

	static OwnerPIN readPin;  // global attribute to handle a PIN code (read permissions)
	static OwnerPIN writePin; // global attribute to handle a PIN code (write permissions)
	static byte activatePinSecurity = (byte)0x01;  // 0x01 = true, 0x00 = false
	static short fileWriteStep = -2; // -2 = not started,
								   // -1 = filename size and filename written to NVR,
								   //  0 = data size written to NVR,
								   //  i = i packets written


	protected TheApplet() {
		// Initialize pin
		byte[] readPincode  = {(byte)0x30, (byte)0x30, (byte)0x30, (byte)0x30}; // PIN code "0000"
		byte[] writePincode = {(byte)0x30, (byte)0x30, (byte)0x30, (byte)0x30}; // PIN code "0000"
		readPin  = new OwnerPIN((byte)3, (byte)8);         // 3 tries, 8=Max Size
		writePin = new OwnerPIN((byte)3, (byte)8);         // 3 tries, 8=Max Size
		readPin.update(readPincode, (short)0, (byte)4);       // from pincode, offset 0, length 4
		writePin.update(writePincode, (short)0, (byte)4);     // from pincode, offset 0, length 4

		this.register();
	}


	public static void install(byte[] bArray, short bOffset, byte bLength) throws ISOException {
		new TheApplet();
	} 


	public boolean select() {
		return true;
	} 


	public void deselect() {
	}


	public void process(APDU apdu) throws ISOException {
		if( selectingApplet() == true )
			return;

		byte[] buffer = apdu.getBuffer();

		switch( buffer[1] )  {
			case UPDATECARDKEY:                  updateCardKey( apdu ); break;
			case UNCIPHERFILEBYCARD:             uncipherFileByCard( apdu ); break;
			case CIPHERFILEBYCARD:               cipherFileByCard( apdu ); break;
			case CIPHERANDUNCIPHERNAMEBYCARD:    cipherAndUncipherNameByCard( apdu ); break;
			case READFILEFROMCARD:               readFileFromCard( apdu ); break;
			case WRITEFILETOCARD:                writeFileToCard( apdu ); break;
			case UPDATEWRITEPIN:                 updateWritePIN( apdu ); break;
			case UPDATEREADPIN:                  updateReadPIN( apdu ); break;
			case DISPLAYPINSECURITY:             displayPINSecurity( apdu ); break;
			case DESACTIVATEACTIVATEPINSECURITY: desactivateActivatePINSecurity( apdu ); break;
			case ENTERREADPIN:                   enterReadPIN( apdu ); break;
			case ENTERWRITEPIN:                  enterWritePIN( apdu ); break;
			case READNAMEFROMCARD:               readNameFromCard( apdu ); break;
			case WRITENAMETOCARD:                writeNameToCard( apdu ); break;
			default: ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED); // bad instruction (fuzzing?)
		}
	}


	void updateCardKey( APDU apdu ) {
	}


	void uncipherFileByCard( APDU apdu ) {
	}


	void cipherFileByCard( APDU apdu ) {
	}


	void cipherAndUncipherNameByCard( APDU apdu ) {
	}


	void readFileFromCard( APDU apdu ) {
		checkReadAccess();

		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();

		switch(buffer[5]) {
			case (byte)0xFE:  // we return the filename
				Util.arrayCopy(NVR, (short)getFilenameOffset(), buffer, (short)0, (short)getFilenameLength());
				apdu.setOutgoingAndSend((short)0, (short)getFilenameLength());
				break;
			case (byte)0xFF:  // we return the file size
				Util.arrayCopy(NVR, (short)getFileSizeOffset(), buffer, (short)0, (short)2);
				apdu.setOutgoingAndSend((short)0, (short)2);
				break;
			default:  // we return file data
				Util.arrayCopy(NVR, (short)(getFileOffset()+buffer[5]*FILE_TRANSFERT_PAYLOAD_SIZE), buffer, (short)0, (short)buffer[6]);
				apdu.setOutgoingAndSend((short)0, (short)buffer[6]);
				break;
		}
	}


	void writeFileToCard( APDU apdu ) {
		checkWriteAccess();

		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();

		switch(fileWriteStep) {
			case -2:  // the packet we're receiving is filename
				Util.arrayCopy(buffer, (short)4, NVR, (short)getFilenameLengthOffset(), (short)1);  // write filename length
				Util.arrayCopy(buffer, (short)5, NVR, (short)getFilenameOffset(), (short)buffer[4]); // write filename
				break;
			case -1:  // the packet we're receiving is file size
				Util.arrayCopy(buffer, (short)5, NVR, (short)getFileSizeOffset(), (short)1);  // high byte
				Util.arrayCopy(buffer, (short)6, NVR, (short)(getFileSizeOffset() + 1), (short)1);  // low byte
				break;
			default:  // the packet we're receiving is file data
				Util.arrayCopy(buffer, (short)5, NVR, (short)(getFileOffset()+fileWriteStep*FILE_TRANSFERT_PAYLOAD_SIZE), (short)buffer[4]);
				break;
		}

		fileWriteStep++;
	}

	short getNameLengthOffset() {
		return (short)0;
	}
	short getNameLength() {
		return (short)NVR[(short)getNameLengthOffset()];
	}
	short getNameOffset() {
		return (short)1;
	}

	short getFilenameLengthOffset() {
		return (short)(getNameOffset() + getNameLength());
	}
	short getFilenameLength() {
		return (short)NVR[(short)getFilenameLengthOffset()];
	}
	short getFilenameOffset() {
		return (short)(getNameOffset() + getNameLength() + 1);
	}

	short getFileSizeOffset() {
		return (short)(getFilenameOffset() + getFilenameLength());
	}
	short getFileSize() {
		byte file_size_high = NVR[(short)getFileSizeOffset()];
		byte file_size_low  = NVR[(short)(getFileSizeOffset() + 1)];
		return (short)((short)((short)file_size_high << 8) + (short)file_size_low);
	}
	short getFileOffset() {
		return (short)(getFilenameOffset() + getFilenameLength() + 1);
	}


	void updateWritePIN( APDU apdu ) {
		checkWriteAccess();

		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();

		byte[] pincode = { (byte)buffer[5], (byte)buffer[6], (byte)buffer[7], (byte)buffer[8] };
		writePin.update(pincode,(short)0,(byte)4);
	}


	void updateReadPIN( APDU apdu ) {
		checkWriteAccess();

		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();

		byte[] pincode = { (byte)buffer[5], (byte)buffer[6], (byte)buffer[7], (byte)buffer[8] };
		readPin.update(pincode,(short)0,(byte)4);
	}


	void displayPINSecurity( APDU apdu ) {
		checkReadAccess();

		byte[] buffer = apdu.getBuffer();
		buffer[0] = (byte)0x00;
		buffer[1] = (byte)activatePinSecurity;
		apdu.setOutgoingAndSend((short)0, (byte)2);
	}


	void desactivateActivatePINSecurity( APDU apdu ) {
		checkWriteAccess();

		if (activatePinSecurity == (byte)0x01)
			activatePinSecurity = (byte)0x00;
		else
			activatePinSecurity = (byte)0x01;
	}


	void enterReadPIN( APDU apdu ) {
		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();
		if( !readPin.check( buffer, (byte)5, buffer[4] ) ) 
			ISOException.throwIt( SW_VERIFICATION_FAILED );
	}


	void enterWritePIN( APDU apdu ) {
		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();
		if( !writePin.check( buffer, (byte)5, buffer[4] ) ) 
			ISOException.throwIt( SW_VERIFICATION_FAILED );
	}


	void readNameFromCard( APDU apdu ) {
		checkReadAccess();

		byte[] buffer = apdu.getBuffer();
		//apdu.setIncomingAndReceive();
		Util.arrayCopyNonAtomic(NVR, (short)1, buffer, (short)0, (short)NVR[0]);
		apdu.setOutgoingAndSend((short)0, (short)NVR[0]);
	}


	void writeNameToCard( APDU apdu ) {
		checkWriteAccess();

		byte[] buffer = apdu.getBuffer();
		apdu.setIncomingAndReceive(); 
		Util.arrayCopy(buffer, (short)4, NVR, (short)0, (short)1);  // write name length
		Util.arrayCopy(buffer, (short)5, NVR, (short)1, (short)buffer[4]); // write name
	}


	void checkReadAccess() throws ISOException {
		if ( ! readPin.isValidated() && activatePinSecurity == 0x01)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
	}

	void checkWriteAccess() throws ISOException {
		if ( ! writePin.isValidated() && activatePinSecurity == 0x01)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
	}

}
