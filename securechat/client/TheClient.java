package client;

import java.io.*;
import opencard.core.service.*;
import opencard.core.terminal.*;
import opencard.core.util.*;
import opencard.opt.util.*;
import java.net.*;
import java.util.*;
import java.lang.System;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;


public class TheClient {
	private boolean encryptedMode = true;

	private PassThruCardService servClient = null;
	boolean DISPLAY = true;

	static final byte CLA                            = (byte)0x00;
	static final byte P1                             = (byte)0x00;
	static final byte P2                             = (byte)0x00;

	// available commands
	private final static byte DES_ECB_NOPAD_ENC               = (byte)0x06;
	private final static byte DES_ECB_NOPAD_DEC               = (byte)0x05;
	private final static byte RSA_DECRYPT                     = (byte)0x04;
	private static final byte GET_PUBLIC_RSA_KEY              = (byte)0x03;
	private static final byte READ_NAME_FROM_CARD             = (byte)0x02;
	private static final byte ENTER_PIN                       = (byte)0x01;

	// logging stuff
	private static String red     = "\u001B[31m";
	private static String green   = "\u001B[32m";
	private static String yellow  = "\u001B[33m";
	private static String blue    = "\u001B[34m";
	private static String magenta = "\u001B[35m";
	private static String cyan    = "\u001B[36m";
	private static String reset = "\u001B[0m";

	private BASE64Encoder base64encoder = new BASE64Encoder();
	private BASE64Decoder base64decoder = new BASE64Decoder();

	static PrintStream out;
	static BufferedReader in;


	/************************************************
	 * *********** BEGINNING OF MAGIC ***************
	 * **********************************************/

	public TheClient() {
		try {
			SmartCard.start();
			System.out.print( "Smartcard inserted?... " ); 

			CardRequest cr = new CardRequest (CardRequest.ANYCARD,null,null); 

			SmartCard sm = SmartCard.waitForCard (cr);

			if (sm != null) {
				System.out.println ("got a SmartCard object!\n");
			} else {
				System.out.println( "did not get a SmartCard object!\n" );
			}

			this.initNewCard( sm ); 

			SmartCard.shutdown();

		} catch( Exception e ) {
			System.out.println( "TheClient error: " + e.getMessage() );
		}
		java.lang.System.exit(0) ;
	}

	private ResponseAPDU sendAPDU(CommandAPDU cmd) {
		return sendAPDU(cmd, true);
	}

	private ResponseAPDU sendAPDU( CommandAPDU cmd, boolean display ) {
		ResponseAPDU result = null;
		try {
			result = this.servClient.sendCommandAPDU( cmd );
			if(display)
				displayAPDU(cmd, result);
		} catch( Exception e ) {
			System.out.println( "Exception caught in sendAPDU: " + e.getMessage() );
			java.lang.System.exit( -1 );
		} 
		return result;
	}

	public static void main( String[] args ) throws InterruptedException {
		new TheClient();
	}


	/************************************************
	 * *********** BEGINNING OF TOOLS ***************
	 * **********************************************/


	private String apdu2string( APDU apdu ) {
		return bytes2string( apdu.getBytes() );
	}

	private String bytes2string( byte[] bytes ) {
		return removeCR( HexString.hexify( bytes ) );
	}


	public void displayAPDU( APDU apdu ) {
		System.out.println( removeCR( HexString.hexify( apdu.getBytes() ) ) + "\n" );
	}


	public void displayAPDU( CommandAPDU termCmd, ResponseAPDU cardResp ) {
		System.out.println( "--> Term: " + removeCR( HexString.hexify( termCmd.getBytes() ) ) );
		System.out.println( "<-- Card: " + removeCR( HexString.hexify( cardResp.getBytes() ) ) );
	}


	private String removeCR( String string ) {
		return string.replace( '\n', ' ' );
	}

	// return the APDU status as boolean
	private boolean apdu2status( ResponseAPDU apdu ) {
		byte[] bytes = apdu.getBytes();
		int length = bytes.length;
		byte[] status = { bytes[length-2], bytes[length-1] };
		String status_s = removeCR( HexString.hexify( status ) );
		if (status_s.equals("90 00"))
			return true;
		else
			return false;
	}

	// return the payload (without the last two bytes (used for status))
	private byte[] apdu2payload( ResponseAPDU apdu ) {
		byte[] bytes = apdu.getBytes();
		if (bytes.length < 3)
			return null;
		byte[] payload = new byte[bytes.length-3];
		System.arraycopy(bytes, 1, payload, 0, bytes.length-3);
		return payload;
	}


	/******************************************
	 * *********** END OF TOOLS ***************
	 * ****************************************/


	private boolean selectApplet() {
		boolean cardOk = false;
		try {
			CommandAPDU cmd = new CommandAPDU( new byte[] {
					(byte)0x00, (byte)0xA4, (byte)0x04, (byte)0x00, (byte)0x0A,
					(byte)0xA0, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x62, 
					(byte)0x03, (byte)0x01, (byte)0x0C, (byte)0x06, (byte)0x01
			} );
			ResponseAPDU resp = this.sendAPDU( cmd );
			if( this.apdu2string( resp ).equals( "90 00" ) )
				cardOk = true;
		} catch(Exception e) {
			System.out.println( "Exception caught in selectApplet: " + e.getMessage() );
			java.lang.System.exit( -1 );
		}
		return cardOk;
	}


	private void initNewCard( SmartCard card ) {
		if( card != null )
			System.out.println( "Smartcard inserted\n" );
		else {
			System.out.println( "Did not get a smartcard" );
			System.exit( -1 );
		}

		System.out.println( "ATR: " + HexString.hexify( card.getCardID().getATR() ) + "\n");


		try {
			this.servClient = (PassThruCardService)card.getCardService( PassThruCardService.class, true );
		} catch( Exception e ) {
			System.out.println( e.getMessage() );
		}

		System.out.println("Applet selecting...");
		if( !this.selectApplet() ) {
			System.out.println( "Wrong card, no applet to select!\n" );
			System.exit( 1 );
			return;
		} else 
			System.out.println( "Applet selected" );

		checkPin();


		// TEST DES
		System.out.println("================= TEST DES ====================");
		sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
        byte[] response;
        byte[] unciphered; 
        long d1, d2, seed=0;
        java.util.Random r = new java.util.Random( seed );

        //byte[] challengeDES = new byte[16];         // size%8==0, coz DES key 64bits
        byte[] challengeDES = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04, 
        					0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, }; // size%8==0, coz DES key 64bits
        challengeDES = addPadding(challengeDES);

        //r.nextBytes( challengeDES );

        //System.out.println( "**TESTING**");
        //testDES_ECB_NOPAD( true );
        //System.out.println( "**TESTING**");
       
        System.out.println("\nchallenge:\n" + encoder.encode(challengeDES) + "\n");
        response = cipherGeneric(DES_ECB_NOPAD_ENC, challengeDES);
        System.out.println("\nciphered is:\n" + encoder.encode(response) + "\n");
        unciphered = cipherGeneric(DES_ECB_NOPAD_DEC, response);
        System.out.print("\nunciphered is:\n" + encoder.encode(unciphered) + "\n");
        System.out.println("================= END TEST DES ====================");
        // END TEST



		try {
			Socket socket = new Socket("localhost", 1111);

			out = new PrintStream(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));


			// launch a thread to get user input
			Thread inputThread = new Thread(new Runnable() {
				@Override
				public void run() {
					Scanner scan = new Scanner(System.in);
					String input = "";
					while (true) {
						//System.out.println("> ");
						input = scan.nextLine().trim();

						if (input.equals(""))
							continue;

						if (input.contains("/help") || input.contains("/list")) {
							out.println(input);
						}

						else if (input.contains("/quit")) {
							out.println(input);
							// TODO: quit client properly
							System.exit(0);
						}

						else if (input.contains("/msg")) {
							String[] s = input.split(" ");
							String targetUser = s[1].trim();
							String message = TheClient.strJoin(Arrays.copyOfRange(s, 2, s.length), " ");
							String message_enc = base64encoder.encode(encrypt(addPadding(message.getBytes())));
							out.println("/msg " + targetUser + " " + message_enc);
						}

						else if (encryptedMode) {
							LogInfo("clair = " + input);

							byte[] clair = input.getBytes();
							byte[] chiffre = encrypt(addPadding(clair));
							LogInfo("chiffre = " + bytes2string(chiffre));
							String chiffre_b64 = base64encoder.encode(chiffre);
							LogInfo("send chiffre b64 = " + chiffre_b64);
							out.println(chiffre_b64);
						}
						else
							out.println(input);
					}
				}
			});
			inputThread.start();


			authenticateWithServer(in, out);
			//messageLoop();
			
		}
		catch (UnknownHostException e) {
			LogFailure("Error: UnknownHostException. Aborting.");
			System.exit(1);
		}
		catch(IOException e) {
			e.printStackTrace();
		}

		
	}

	/**************************************************************
	 * **** beginning of custom stuff        ***************
	 * ************************************************************/

	/**************************************************************
	 * ****          network stuff        ***************
	 * ************************************************************/


	private void checkPin() {
		// check PIN
		boolean true_ = true;
		while(true_) {
			boolean enterPinStatus = enterPIN();
			if (enterPinStatus) {
				LogSuccess("PIN success");
				break;
			}
			else
				LogFailure("PIN fail");
		}
	}

	private void authenticateWithServer(BufferedReader in, PrintStream out) throws IOException {
		// get name from card
		String name = readNameFromCard();
		LogDebug(name);

		boolean true_ = true;
		while(true_) {
			String msg = in.readLine();
			
			if (msg.contains("Choose a pseudo:")) {
				System.out.println(msg);
				LogInfo("sending: " + name);
				out.println(name);
			}

			else if (msg.contains("Send RSA modulus:")) {
				System.out.println(msg);
				String modulus = getPublicRSAModulus().replaceAll("\r\n", "");
				LogInfo("sending: " + modulus);
				out.println(modulus);
			}

			else if (msg.contains("Send RSA exponent:")) {
				System.out.println(msg);
				String exponent = getPublicRSAExponent().replaceAll("\r\n", "");
				LogInfo("sending: " + exponent);
				out.println(exponent);
			}

			else if (msg.contains("Sending challenge:")) {
				System.out.println(msg);
				String challenge_b64 = msg.split(":")[1].trim();
				byte[] challenge = base64decoder.decodeBuffer(challenge_b64);
				LogInfo("challenge = " + bytes2string(challenge));
				LogInfo("challenge length = " + challenge.length);
				
				String challenge_dec_b64 = solveChallenge(challenge);
				out.println(challenge_dec_b64);
			}

			else if (msg.contains("===") || msg.contains("/help") || msg.contains("/list") || msg.contains("/quit")) {
				System.out.println(msg);
			}

			else {
				if (encryptedMode) {
					LogInfo("recu message chiffre = " + msg);
					String chiffre_b64 = msg.split(" ")[1].trim();
					LogInfo("partie chiffre b64 = " + chiffre_b64);
					byte[] chiffre_bytes = base64decoder.decodeBuffer(chiffre_b64);
					LogInfo("bytes chiffre = " + bytes2string(chiffre_bytes));
					// what the fuck: we have to suppress last 3 bytes
					// that are added I don't know why by base64decoder.decodeBuffer()
					int a = (chiffre_bytes.length / 8);
					byte[] fuck = new byte[a*8];
					System.arraycopy(chiffre_bytes, 0, fuck, 0, a*8);
					// end fuck
					byte[] clair_bytes = decrypt(fuck);
					String clair = new String(clair_bytes, "UTF-8");
					LogInfo("clair decode = " + clair);

					System.out.println(msg.split(" ")[0] + " " + clair);
				}
				else {
					LogInfo("recu message clair = " + msg);
					System.out.println(msg);
				}
			}
		}
	}


	/**************************************************************
	 * ****          Communication with card stuff        ***************
	 * ************************************************************/

	// padding
	private byte[] addPadding(byte[] input) {
		int pad_to_add = 8 - (input.length % 8);
		if (pad_to_add == 0)
			return input;
		byte[] input_padded = new byte[input.length + pad_to_add];
		// arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
		System.arraycopy(input, 0, input_padded, 0, input.length);
		for (int i=0; i<pad_to_add; i++) {
			input_padded[input.length+i] = 0x20;
		}
		return input_padded;
	}

	// DES stuff
	private byte[] encrypt(  byte[] challenge ) { 
		return cipherGeneric( DES_ECB_NOPAD_ENC, challenge );
	}

	private byte[] decrypt(  byte[] challenge ) { 
		return cipherGeneric( DES_ECB_NOPAD_DEC, challenge );
	}

    private byte[] cipherGeneric( byte typeINS, byte[] challenge ) {
        byte[] header = { CLA, typeINS, P1, P2, (byte)challenge.length }; 
        byte[] cmd = new byte[header.length + challenge.length + 1];

        System.arraycopy(header, (byte)0, cmd, (byte)0, (byte)5);
        System.arraycopy(challenge, (byte)0, cmd, (byte)5, (byte)challenge.length);
        cmd[cmd.length-1] = (byte)challenge.length;

        CommandAPDU cmd_apdu = new CommandAPDU( cmd );
        ResponseAPDU resp = this.sendAPDU( cmd_apdu );

        byte[] result = new byte[challenge.length];
        System.arraycopy(resp.getBytes(), 0, result, 0, resp.getBytes().length-2);
        return result;
    }
    

	// RSA stuf (challenge solving)
	private String solveChallenge(byte[] challenge) {
		byte[] header =  { CLA,  RSA_DECRYPT, P1, P2, (byte)challenge.length };
		byte[] cmd = new byte[header.length + challenge.length];

		System.arraycopy(header,    (byte)0, cmd, (byte)0,             header.length);
		System.arraycopy(challenge, (byte)0, cmd, (byte)header.length, challenge.length);

		CommandAPDU cmd_apdu = new CommandAPDU(cmd);
		ResponseAPDU resp = sendAPDU(cmd_apdu);

		// second request (magic by Yohann a.k.a. JavaBoy)
		header = new byte[] { (byte)0x90, (byte)0xc0, P1, P2, (byte)0x00 };
		cmd_apdu = new CommandAPDU(header);
		resp = sendAPDU(cmd_apdu);

		byte[] challenge_dec = apdu2payload(resp);
		String challenge_dec_b64 = base64encoder.encode(challenge_dec);
		challenge_dec_b64 = challenge_dec_b64.replaceAll("\r\n", "");

		return challenge_dec_b64;
	}

	// RSA stuff (public key sending)
	private String getPublicRSAModulus() {
		return getPublicRSA_((byte)0x00);
	}

	private String getPublicRSAExponent() {
		return getPublicRSA_((byte)0x01);
	}

	private String getPublicRSA_(byte p2) {
		byte[] cmd = { CLA,  GET_PUBLIC_RSA_KEY, P1, p2, (byte)0x00 };
		CommandAPDU cmd_apdu = new CommandAPDU( cmd );
		ResponseAPDU resp = sendAPDU(cmd_apdu);

		if (!apdu2status(resp))
			LogFailure("getting modulus or exponent failed.");

		byte[] pubkey = apdu2payload( resp );

		if (p2 == 0x00)
			LogDebug("modulus = " + bytes2string(pubkey));
		else
			LogDebug("exponent = " + bytes2string(pubkey));

		return base64encoder.encode(pubkey);
	}

	// name stuff
	private String readNameFromCard() {
		byte[] cmd = { CLA, READ_NAME_FROM_CARD, P1, P2, 0x00 };
		CommandAPDU cmd_apdu = new CommandAPDU( cmd );
		ResponseAPDU resp = this.sendAPDU( cmd_apdu, DISPLAY );

		byte[] bytes = resp.getBytes();
		String msg = "";
		for(int i=0; i<bytes.length-2;i++)
			msg += new StringBuffer("").append((char)bytes[i]);
		return msg;
	}

	// PIN stuff
	private boolean enterPIN() {
		ResponseAPDU resp = fromDisplayToSendAPDU(ENTER_PIN, "Enter PIN: ");
		if (resp == null)
			return false;
		return apdu2status(resp);
	}

	private ResponseAPDU fromDisplayToSendAPDU(byte ins, String message) {
		System.out.print(message);
		String data = readKeyboard();
		if (data == null) {
			System.out.println("error: null user input from keyboard, aborting");
			return null;
		}

		byte[] data_b = data.getBytes();
		byte[] header = { CLA, ins, P1, P2, (byte)data_b.length };
		byte[] cmd = new byte[header.length + data_b.length];

		System.arraycopy(header, (byte)0, cmd, (byte)0, header.length);
		System.arraycopy(data_b, (byte)0, cmd, (byte)header.length, data_b.length);

		CommandAPDU cmd_apdu = new CommandAPDU( cmd );
		ResponseAPDU resp = this.sendAPDU( cmd_apdu, DISPLAY );

		return resp;
	}

	private String readKeyboard() {
		String result = null;

		try {
			BufferedReader input = new BufferedReader( new InputStreamReader( System.in ) );
			result = input.readLine();
		} catch( Exception e ) {}

		return result;
	}






	/**************************************************************
	 * ****          Logging stuff        ***************
	 * ************************************************************/

	private static void LogSuccess(String s) {
		System.out.println(green + s + reset);
	}

	private static void LogFailure(String s) {
		System.out.println(red + s + reset);
	}

	private static void LogInfo(String s) {
		System.out.println(cyan + "[INFO] " + s + reset);
	}

	private static void LogDebug(String s) {
		System.out.println(yellow + "[DEBUG] " + s + reset);
	}






	public static String strJoin(String[] aArr, String sSep) {
	    StringBuilder sbStr = new StringBuilder();
	    for (int i = 0, il = aArr.length; i < il; i++) {
	        if (i > 0)
	            sbStr.append(sSep);
	        sbStr.append(aArr[i]);
	    }
	    return sbStr.toString();
	}
}
